Name:           autoinit
Version:        0.1.3
Release:        1%{?dist}
Summary:        Minimal automotive initrd
URL:            https://gitlab.com/CentOS/automotive/src/autoinit
License:        LGPL-2.0-or-later
Source0:        autoinit-%{version}.tar.xz

BuildRequires:  meson
BuildRequires:  gcc
Requires:       ostree

%description
Minimal automotive initrd

%prep
%autosetup

%build
%meson
%meson_build

%check
%meson_test

%install
%meson_install

%files
%{_prefix}/lib/dracut/modules.d/50autoinit
%{_prefix}/lib/autoinit/autoinit

%changelog
* Thu Apr 25 2024 Alexander Larsson <alexl@redhat.com> - 0.1.3-1
- Update to 0.1.3

* Thu Apr 18 2024 Alexander Larsson <alexl@redhat.com>
- Initial version
